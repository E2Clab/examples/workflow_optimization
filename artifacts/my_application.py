import time
import argparse
import ast

parser = argparse.ArgumentParser()
parser.add_argument(
    "--config",
    type=str,
    required=True,
    help="Application configuration suggested by the optimization algorithm",
)
args = parser.parse_args()

_config = ast.literal_eval(args.config)


print(f" ******* optimization config = {_config}")
workload_size = 100
communication_cost = 2
user_response_time = \
    _config['num_workers'] * communication_cost + \
    workload_size/(_config['cores_per_worker']*_config['num_workers']) + \
    workload_size/(_config['memory_per_worker']*_config['num_workers'])

print(" Running...")
time.sleep(user_response_time)
print(f" ******* user_response_time = {user_response_time}")

with open('results.txt', 'w') as f:
    f.write(f'{user_response_time}')
