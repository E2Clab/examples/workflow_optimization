import yaml
from pathlib import Path

from ray import tune, train

from ray.tune.schedulers import AsyncHyperBandScheduler
from ray.tune.search import ConcurrencyLimiter
from ray.tune.search.hyperopt import HyperOptSearch

from e2clab.optimizer import Optimizer


class UserDefinedOptimization(Optimizer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    MAX_CONCURRENCY = 3
    NUM_SAMPLES = 9

    # 'run' abstract method to define
    def run(self):
        algo = HyperOptSearch()
        algo = ConcurrencyLimiter(algo, max_concurrent=self.MAX_CONCURRENCY)
        scheduler = AsyncHyperBandScheduler()
        objective = tune.run(
            self.run_objective,
            metric="user_response_time",
            mode="min",
            name="my_application",
            search_alg=algo,
            scheduler=scheduler,
            num_samples=self.NUM_SAMPLES,
            config={
                "num_workers": tune.randint(1, 10),
                "cores_per_worker": tune.randint(20, 50),
                "memory_per_worker": tune.randint(1, 3),
            },
            fail_fast=True,
        )

        print("Hyperparameters found: ", objective.best_config)

    # Function to optimize
    def run_objective(self, _config):
        # create an optimization directory using "self.prepare()"
        # accessible in 'self.optimization_dir'
        optimization_dir = self.prepare()

        # update the parameters of your configuration file(s)
        # (located in "self.optimization_dir") according to
        # "_config" (defined by the search algorithm)
        with open(f"{optimization_dir}/layers_services.yaml") as f:
            config_yaml = yaml.load(f, Loader=yaml.FullLoader)
        for layer in config_yaml["layers"]:
            for service in layer["services"]:
                if service["name"] in ["myapplication"]:
                    service["quantity"] = _config["num_workers"]
        with open(f"{optimization_dir}/layers_services.yaml", "w") as f:
            yaml.dump(config_yaml, f)

        # deploy the configurations using "self.launch()".
        # "self.launch()" runs:
        #   layers_services;
        #   network;
        #   workflow (prepare, launch, finalize);
        #   finalize;
        # returns the 'result_dir'(Path) where you can access
        # artifacts pulled from your experiment during the 'finalize' step
        result_dir = self.launch(optimization_config=_config)

        # Move the optimization results from your experiment folder to
        # your optimization folder and destroy computing resources
        result_file = "results/results.txt"
        result_file = f"{result_dir}/{result_file}"
        with open(result_file) as file:
            line = file.readline()
            user_response_time = float(line.rstrip())

        # report the metric value to Ray Tune
        train.report({"user_response_time": user_response_time})

        # Free computing resources
        self.finalize()


if __name__ == "__main__":
    # Programmatically run optimization
    optimizer = UserDefinedOptimization(
        scenario_dir=Path(".").resolve(),
        artifacts_dir=Path("./artifacts/").resolve(),
        duration=0,
        repeat=0,
    )
    optimizer.run()
